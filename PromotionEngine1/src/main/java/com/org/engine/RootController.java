package com.org.engine;

import java.util.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RootController {

	Map<String, Integer> skuIdMap = new HashMap<String, Integer>();
	Map<String, Integer> activePromotions = new HashMap<String, Integer>();
	Map<String, Integer> promotions = new HashMap<String, Integer>();

	@RequestMapping("/")
	public String home() {
		System.out.println("RootController.home");

		skuIdMap.put("A", 50);
		skuIdMap.put("B", 30);
		skuIdMap.put("C", 20);
		skuIdMap.put("D", 15);

		promotions.put("A", 3);
		promotions.put("B", 2);
		promotions.put("C+D", 1);

		// more promotion types can be added here at a later date
		activePromotions.put("3*A", 130);// 3*A
		activePromotions.put("2*B", 45);// 2*B
		activePromotions.put("C+D", 30);// C+D

		return "index";
	}

	@RequestMapping("/scenarioA")
	public String scenarioA(Model m) {
		System.out.println("RootController.scenarioA");
		Map<String, Integer> a = new HashMap<String, Integer>();
		a.put("A", 1);
		a.put("B", 1);
		a.put("C", 1);
		int result = promotionEngine(a);
		System.out.println("result:"+result);
		m.addAttribute("result", result);
		return "A";
	}

	@RequestMapping("/scenarioB")
	public String scenarioB(Model m) {
		System.out.println("RootController.scenarioB");
		Map<String, Integer> b = new HashMap<String, Integer>();
		b.put("A", 5);
		b.put("B", 5);
		b.put("C", 1);
		int result = promotionEngine(b);
		System.out.println("result:"+result);
		m.addAttribute("result", result);
		return "B";
	}

	@RequestMapping("/scenarioC")
	public String scenarioC(Model m) {
		System.out.println("RootController.scenarioC");
		Map<String, Integer> c = new HashMap<String, Integer>();
		c.put("A", 3);
		c.put("B", 5);
		c.put("C", 1);
		c.put("D", 1);
		int result = promotionEngine(c);
		System.out.println("result:"+result);
		m.addAttribute("result", result);
		return "C";
	}

	int promotionEngine(Map<String, Integer> scenarioMap) {
		int count = 0;

		if (scenarioMap.get("A") == 1) {
			count = count + skuIdMap.get("A");
		} else {
			int quotient = scenarioMap.get("A") / promotions.get("A");
			count = count + quotient * activePromotions.get(promotions.get("A") + "*" + "A");
			int reminder = scenarioMap.get("A") % promotions.get("A");
			count = count + reminder * skuIdMap.get("A");
		}
		if (scenarioMap.get("B") == 1) {
			count = count + skuIdMap.get("B");
		} else {
			int quotient = scenarioMap.get("B") / promotions.get("B");
			count = count + quotient * activePromotions.get(promotions.get("B") + "*" + "B");
			int reminder = scenarioMap.get("B") % promotions.get("B");
			count = count + reminder * skuIdMap.get("B");
		}

		if (scenarioMap.get("C") == 1) {
			count = count + skuIdMap.get("C");
		} else {
//			int quotient = scenarioMap.get("C") / promotions.get("C");
//			count = count + quotient * activePromotions.get("C+D");
//			int reminder = scenarioMap.get("C") % promotions.get("C");
//			count = count + reminder * skuIdMap.get("C");
		}

		if (scenarioMap.get("D") != null && scenarioMap.get("D") == 1) {
			count = count + skuIdMap.get("D");
		}
		return count;

	}
}
